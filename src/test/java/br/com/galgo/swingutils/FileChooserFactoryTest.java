/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.swingutils;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class FileChooserFactoryTest {

    private static final Logger log = LoggerFactory.getLogger(FileChooserFactoryTest.class);
    private static JFrameTest frame;

    @BeforeClass
    public static void beforeClass() {
        log.debug("Instanciando um JFrame");
        frame = new JFrameTest();
        frame.setVisible(true);
    }

    @Test
    public void a() {
        log.debug("*******************************************************************");
        try {
            JFileChooser fileChooser = FileChooserFactory.newSaveXML(null);
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            }
            fileChooser = FileChooserFactory.newSaveXML(new File(
                    "c:/Users/valdemar.arantes/STI/13 - Aplicativos/"
                            + "11 - Validador de Layout de Extrato/"
                            + "Validador de Layout de Extrato - Requisitos.docx"));
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            }
            fileChooser = FileChooserFactory.newSaveXML(new File(
                    "c:/Users/valdemar.arantes/pasta inexistente"));
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            }
            Assert.assertTrue(true);
        } catch (Exception e) {
            Assert.fail("Exceção inesperada");
        }
    }
}
