/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.swingutils;

import java.awt.Cursor;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;

/**
 * Gerencia o visual do aplicativo quando um processo longo está em execução.
 *
 * @author valdemar.arantes
 */
public class LongProcessHandler {

    private final static MouseAdapter doNothingMouseAdapter = new MouseAdapter() {
    };
    private final static KeyAdapter doNothingKeyAdapter = new KeyAdapter() {
    };
    private JFrame frame;

    public LongProcessHandler(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Bloqueia o JFrame para ações de mouse e teclado e troca o cursor para Espera
     */
    public void beforeLongProcess() {
        frame.getGlassPane().addMouseListener(doNothingMouseAdapter);
        frame.getGlassPane().addKeyListener(doNothingKeyAdapter);
        frame.getGlassPane().setVisible(true);
        frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

    }

    /**
     * Desbloqueia o JFrame para ações de mouse e teclado e troca o cursor para Padrão.
     * Também apresenta uma janela popup com uma mensagem de conclusão do processo.
     */
    public void afterLongProcess() {
        frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        frame.getGlassPane().setVisible(false);
        frame.getGlassPane().removeMouseListener(doNothingMouseAdapter);
        frame.getGlassPane().removeKeyListener(doNothingKeyAdapter);
        JOptionPane.showMessageDialog(frame.getContentPane(), "Operação concluída", "Mensagem",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
